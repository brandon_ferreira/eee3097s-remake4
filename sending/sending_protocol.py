#--------------------------------------------------------------------------------
# Authors: Brandon Ferreira and Sina Davachi Omoumi
# Student Numbers: FRRBRA002 and DVCSIN001
# DESCRIPTION:
# The following code reads data from a sensor HAT for the raspberry pi, the code
# then stores this data into an array.  The array data is then compressed and
# encrypted using zlib and AES libraries respectively.
#--------------------------------------------------------------------------------
# Compression and Encryption Imports
#--------------------------------------------
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
import binascii
import zlib
import sys

import time
#--------------------------------------------
# Sensor Imports
#--------------------------------------------
from AD import ADS1015
import AD
from ICM20948 import ICM
import ICM20948
import math
import ctypes
#--------------------------------------------
# SHTC3 Library
#--------------------------------------------
class SHTC3:
    def __init__(self):
        self.dll = ctypes.CDLL("./SHTC3.so")
        init = self.dll.init
        init.restype = ctypes.c_int
        init.argtypes = [ctypes.c_void_p]
        init(None)

    def SHTC3_Read_Temperature(self):
        temperature = self.dll.SHTC3_Read_TH
        temperature.restype = ctypes.c_float
        temperature.argtypes = [ctypes.c_void_p]
        return temperature(None)

    def SHTC3_Read_Humidity(self):
        humidity = self.dll.SHTC3_Read_RH
        humidity.restype = ctypes.c_float
        humidity.argtypes = [ctypes.c_void_p]
        return humidity(None)
#--------------------------------------------
# Compression and Encryption functions
#--------------------------------------------
def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def compress(plaintext):
  comp = zlib.compress(plaintext.encode("utf-8"),7)
  return comp

def encrypt(plaintext, key, iv):
  data_bytes = plaintext
  padded_bytes = pad(data_bytes, AES.block_size)

  AES_object = AES.new(key, AES.MODE_CBC, iv)
  ciphertext = AES_object.encrypt(padded_bytes)
  return ciphertext
#--------------------------------------------
# Main function
#--------------------------------------------
def main():
  #------------------------------------------
  # Data variable declaration
  #------------------------------------------
  output_list = []
  #rows = []
  rows = [" "]*100
  comp_rows = []

  comp_times = []
  e_times = []

  #------------------------------------------
  testVar = [" "]*100
  #------------------------------------------
  # Measurements
  #------------------------------------------
  # Library instantiation
  ads1015=ADS1015()
  state=ads1015._read_u16(AD.ADS_POINTER_CONFIG) & 0x8000
  # ICM20948
  icm20948=ICM()
  # SHTC3
  shtc3 = SHTC3()
  # Error handeling
  if(state!=0x8000):
      print("\nADS1015 Error\n")
  # Reading
  index = 0
  print("Sample size is: ",100)
  while (index<100):
        #----------------------------------------------
        # ICM20948 Measurements
        #----------------------------------------------
        icm20948.icm20948_Gyro_Accel_Read()
        icm20948.icm20948MagRead()
        icm20948.icm20948CalAvgValue()
        time.sleep(1)

        icm20948.imuAHRSupdate(ICM20948.MotionVal[0] * 0.0175,ICM20948.MotionVal[1] * 0.0175,ICM20948.MotionVal[2] * 0.0175,ICM20948.MotionVal[3],ICM20948.MotionVal[4],ICM20948.MotionVal[5],ICM20948.MotionVal[6],ICM20948.MotionVal[7],ICM20948.MotionVal[8])
        pitch = math.asin(-2 * ICM20948.q1 * ICM20948.q3 + 2 * ICM20948.q0* ICM20948.q2)* 57.3
        roll  = math.atan2(2 * ICM20948.q2 * ICM20948.q3 + 2 * ICM20948.q0 * ICM20948.q1, -2 * ICM20948.q1 * ICM20948.q1 - 2 * ICM20948.q2* ICM20948.q2 + 1)* 57.3
        yaw   = math.atan2(-2 * ICM20948.q1 * ICM20948.q2 - 2 * ICM20948.q0 * ICM20948.q3, 2 * ICM20948.q2 * ICM20948.q2 + 2 * ICM20948.q3 * ICM20948.q3 - 1) * 57.3

        accelx = (ICM20948.Accel[0]/16384)*9.8
        accely = (ICM20948.Accel[1]/16384)*9.8
        accelz = (ICM20948.Accel[2]/16384)*9.8
        gyrox = ICM20948.Gyro[0]
        gyroy = ICM20948.Gyro[1]
        gyroz = ICM20948.Gyro[2]
        magx = ICM20948.Mag[0]
        magy = ICM20948.Mag[1]
        magz = ICM20948.Mag[2]
        #----------------------------------------------
        # SHTC3 Measurements
        #----------------------------------------------
        temperature = shtc3.SHTC3_Read_Temperature()
        humidity = shtc3.SHTC3_Read_Humidity()
        #----------------------------------------------
        # Output format
        #----------------------------------------------
        stringOut = str(accelx) + "," + str(accely) + "," + str(accelz) + "," + str(gyrox) + "," + str(gyroy) + "," + str(gyroz) + "," + str(magx) + "," + str(magy) + "," + str(magz)
        stringOut = stringOut + "," + str(temperature) + "," + str(humidity) + "\n"

        print("Measurement: ",index)

        #rows.append(stringOut)
        rows[index] = stringOut

        testVar[index] = str(humidity) + "\n"
        print(testVar[index])
        index = index + 1
  #------------------------------------------
  # Output Data File
  #------------------------------------------
  data_file = open("DATA.txt", "w")
  for i in range(len(rows)):
    data_file.write(rows[i])
  data_file.close()
  #------------------------------------------
  # Test Data
  #------------------------------------------
  data_file = open("Humidity.txt", "w")
  for i in range(len(testVar)):
    data_file.write(testVar[i])
  data_file.close()
  #------------------------------------------
  # Compression
  #------------------------------------------
  print("-------------------------------------------")
  #starttime = time.time_ns()/1000000000
  for lines in rows:
      comp_rows.append(compress(lines))
  #endtime = time.time_ns()/1000000000
  #print("Compression time is: ",endtime-starttime)
  #comp_times.append(endtime-starttime)
  #------------------------------------------
  # Compressed Output file
  #------------------------------------------
  comp_file = open("Compressed.txt", "wb")
  for i in range(len(comp_rows)):
    comp_file.write(comp_rows[i])
  comp_file.close()
  #------------------------------------------
  x=0
  data=" "
  while(x<5):
      for line in rows:
           compress(line)
      x=x+1
  x=0
  while(x<10):
      starttime = time.time_ns()/1000000000
      for line in rows:
          compress(line)
      endtime = time.time_ns()/1000000000
      print("Compression time is: ",endtime-starttime)
      comp_times.append(endtime-starttime)
      x=x+1
  #------------------------------------------
  print("-------------------------------------------")
  #------------------------------------------
  # Encryption
  #------------------------------------------
  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  #starttime = time.time_ns()/1000000000
  for line in comp_rows:
    output_list.append(encrypt(line, key, iv))

  #endtime = time.time_ns()/1000000000
  #print("Encryption time is: ",endtime-starttime)
  #e_times.append(endtime-starttime)
  #------------------------------------------
  # Final Output to be sent
  #------------------------------------------
  output_file = open("encrypted_compressed.txt", "w")
  for i in range(len(output_list)):
    output_file.write(binascii.hexlify(output_list[i]).decode("utf-8")+"\n")
  output_file.close()
  #------------------------------------------
  x=0
  data=" "
  while(x<5):
      for line in comp_rows:
           encrypt(line, key, iv)
      x=x+1
  x=0
  while(x<10):
      starttime = time.time_ns()/1000000000
      for line in comp_rows:
          encrypt(line, key, iv)
      endtime = time.time_ns()/1000000000
      print("Compression time is: ",endtime-starttime)
      comp_times.append(endtime-starttime)
      x=x+1
  #------------------------------------------
  # Final Output to be sent
  #------------------------------------------
  output_file = open("comp_times.txt", "w")
  for i in range(len(comp_times)):
    output_file.write(str(comp_times[i])+"\n")
  output_file.close()

  output_file = open("encrypt_times.txt", "w")
  for i in range(len(e_times)):
    output_file.write(str(e_times[i])+"\n")
  output_file.close()


#--------------------------------------------
# Start Code
#--------------------------------------------
if __name__ == "__main__":
  main()
#--------------------------------------------
# END OF CODE
#--------------------------------------------
