from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
import binascii
import zlib
import sys



def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def compress(plaintext):
  comp = zlib.compress(plaintext.encode("utf-8"), 2)
  return comp

def encrypt(plaintext, key, iv):
  data_bytes = plaintext
  padded_bytes = pad(data_bytes, AES.block_size)

  AES_object = AES.new(key, AES.MODE_CBC, iv)
  ciphertext = AES_object.encrypt(padded_bytes)
  return ciphertext

def main():
  output_list = []
  rows = []
  comp_rows = []

  input_list = read_input("Test.csv")

  for line in input_list:
    comp_rows.append(compress(line))
  
  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  for line in comp_rows:
    output_list.append(encrypt(line, key, iv))

  output_file = open("encrypted_compressed.txt", "w")
  for i in range(len(output_list)):
    output_file.write(binascii.hexlify(output_list[i]).decode("utf-8")+"\n")
  output_file.close()

if __name__ == "__main__":
  main()
