import zlib
import sys
import csv

input_data = open("Test.csv")
csvreader = csv.reader(input_data)
rows = []
comp_rows = []

header = next(csvreader)
print(header)

for row in csvreader:
     data = row
     tempVar = ""
     for element in data:
          tempVar = tempVar + element + " "
     comp = zlib.compress(tempVar.encode('utf-8'))
     rows.append(tempVar)
     comp_rows.append(comp)

input_data.close()


print(rows[2])
print(comp_rows[2])
tempVar = zlib.decompress(comp_rows[2]).decode('utf-8')
print(tempVar)

print(rows[400])
print(comp_rows[400])
tempVar = zlib.decompress(comp_rows[400]).decode('utf-8')
print(tempVar)

print(rows[652])
print(comp_rows[652])
tempVar = zlib.decompress(comp_rows[652]).decode('utf-8')
print(tempVar)

print("Size of original data: ", sys.getsizeof(rows[2]))
print("Size of compressed data: ", sys.getsizeof(comp_rows[2]))

print("Size of original data: ", sys.getsizeof(rows[400]))
print("Size of compressed data: ", sys.getsizeof(comp_rows[400]))

print("Size of original data: ", sys.getsizeof(rows[652]))
print("Size of compressed data: ", sys.getsizeof(comp_rows[652]))
