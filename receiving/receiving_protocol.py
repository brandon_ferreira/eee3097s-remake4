from Crypto.Util.Padding import pad, unpad
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
import binascii
import zlib
import sys

import time

def decompress(plaintext):
  decomp = zlib.decompress(plaintext)
  return decomp

def read_input(filename):
  input_file = open(filename, "r")

  input_list = []
  for line in input_file:
    stripped_line = line.strip()
    input_list.append(stripped_line)

  input_file.close()
  return input_list

def decrypt(ciphertext, key, iv):
  data_bytes = binascii.unhexlify(ciphertext)
  AES_object = AES.new(key, AES.MODE_CBC, iv)
  raw_bytes = AES_object.decrypt(data_bytes)
  extracted_bytes = unpad(raw_bytes, AES.block_size)
  return extracted_bytes

def main():
  input_list = read_input("encrypted_compressed.txt")
  output_list = []
  decrypt_list = []

  key = pad(b"specialkey", AES.block_size)
  iv = pad(b"specialiv", AES.block_size)

  for line in input_list:
    decrypt_list.append(decrypt(line, key, iv))
  
  for line in decrypt_list:
    output_list.append(decompress(line))
  #---------------------------------------------------------------
  # Time Tests
  #---------------------------------------------------------------
  # Decrypt
  #---------------------------------------------------------------
  comp_times = []
  e_times = []
  x=0

  while(x<5):
      for line in input_list:
           decrypt(line,key,iv)
      x=x+1
  x=0
  while(x<10):
      starttime = time.time_ns()/1000000000
      for line in input_list:
           decrypt(line,key,iv)
      endtime = time.time_ns()/1000000000
      print("Decryption time is: ",endtime-starttime)
      e_times.append(endtime-starttime)
      x=x+1
  #---------------------------------------------------------------
  # Decompression
  #---------------------------------------------------------------
  while(x<5):
      for line in decrypt_list:
           decompress(line)
      x=x+1
  x=0
  while(x<10):
      starttime = time.time_ns()/1000000000
      for line in decrypt_list:
           decompress(line)
      endtime = time.time_ns()/1000000000
      print("Compression time is: ",endtime-starttime)
      comp_times.append(endtime-starttime)
      x=x+1
  #---------------------------------------------------------------
  # Time output files
  #---------------------------------------------------------------
  output1_file = open("decomp_times.txt", "w")
  for i in range(len(comp_times)):
    output1_file.write(str(comp_times[i])+"\n")
  output1_file.close()

  output2_file = open("decrypt_times.txt", "w")
  for i in range(len(e_times)):
    output2_file.write(str(e_times[i])+"\n")
  output2_file.close()

  #---------------------------------------------------------------

  output_file = open("decrypted_decompressed.csv", "w")
  for i in range(len(output_list)):
    output_file.write(output_list[i].decode("utf-8")+"\n")
  output_file.close()

  with open('DATA.txt','r') as file1:
     with open('decrypted_decompressed.csv','r') as file2:
         difference = set(file1).difference(file2)
  difference.discard('\n')
  with open('diff.txt','w') as file_out:
      for line in difference:
          file_out.write(line)
  file_out.close()

if __name__ == "__main__":
  main()
